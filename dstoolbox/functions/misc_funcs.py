from math import ceil
from random import randint

def round_up_nearest(val: float, nearest: float):
    """Round to the nearst value, i.e. 10 or 5
    
    Parameters
    ----------
    val : float
        Value to round up.
    nearest : float
        Nearest value to round to.
    
    Returns
    -------
    float
    """
    return nearest * ceil(val / nearest)


def unique_random(_min, _max, _list):
    """[summary]
    
    Parameters
    ----------
    _min : [type]
        [description]
    _max : [type]
        [description]
    _list : [type]
        [description]
    
    Returns
    -------
    [type]
        [description]
    """
    x = randint(_min, _max)
    while x in _list:
        x = randint(_min, _max)
    return x
    