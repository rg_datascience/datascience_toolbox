import datetime
import re
import warnings
import numpy as np
import pandas as pd
import holidays


warnings.simplefilter('once', DeprecationWarning)

def get_monday_of_week(date: datetime.date = None, year: int = None, week: int = None):
    """
    Get the date for the monday of a given year and week or from a date.
    Pass either date OR (year and week).

    *DEPRECATED* - Use **get_weekof_day**

    Parameters
    ----------
    date : datetime.date, optional
        Date to use to get the reference date. The default is None.
    
    year : int, optional
        Year integer (YYYY format). The default is None. 

    week : int, optional
        Week integer. The default is None. 

    Returns
    -------
    datetime.date
    
    """
    try:
        warnings.warn('get_monday_of_week is deprecated. Use get_weekof_day instead', 
            DeprecationWarning)
        if date is not None:
            year = date.isocalendar()[0]
            week = date.isocalendar()[1]
            
        yw = str(year) + '-W' + str(week-1)
        monday = datetime.datetime.strptime(yw + '-1', '%Y-W%W-%w').date()
        return monday
    except Exception as e:
        print(e)


def get_weekof_day(date: datetime.date = None, year: int = None, week: int = 1, 
                   weekday:int = 1):
    """
    Get the date for the monday of a given year and week or from a date.
    Pass either date OR (year and week).
    

    Parameters
    ----------
    date : datetime.date, optional
        Date to use to get the reference date. The default is None.
    
    year : int, optional
        Year integer (YYYY format). The default is None. 

    week : int, optional
        Week integer. The default is None.
        
    weekday : int, optional
        Day of week to return. The default is 1 (Monday). Sunday is 0, Friday
        is 5, and so on.

    Returns
    -------
    datetime.date
    
    """
    try:
        if date is not None:
            year = date.isocalendar()[0]
            week = date.isocalendar()[1]
        
        if 0 < weekday < 7:
            wkday = str(weekday * -1)
        else:
            week -= 1
            wkday = '-0'
        
        yw = str(year) + '-W' + str(week-1)
        
        woday = datetime.datetime.strptime(yw + wkday, '%Y-W%W-%w').date()
        return woday
    except Exception as e:
        print(e)


def next_weekday(date: datetime.date = None, offset: int = 0, weekday: int = 0):
    """
    Get the date of the next weekday from the date passed.

    Parameters
    ----------
    date : datetime.date, optional
        Date used for reference. If none, the current date is used. The default is None.
    
    offset : int, optional
        Additional offset in days to add or remove from 7. The default is 0.
    
    weekday : int, optional
        0 = Monday, 1=Tuesday, 2=Wednesday... The default is 0.

    Returns
    -------
    datetime.date
    """
    try:
        if date == None:
            date = datetime.date.today()
            
        days_ahead = weekday - date.weekday()
        if days_ahead <= 0: # Target day already happened this week
            days_ahead += 7 + offset # 0 = Monday, 1=Tuesday, 2=Wednesday...
        return date + datetime.timedelta(days_ahead)
    except Exception as e:
        print(e)


class AdjacentHoliday():

    def __init__(self, min_date, max_date, countries: list = ['US', 'CA'], 
                 within_n_days: int = 1):
        """Tool to check if a date is adjacent to a holiday. Useful when dates
        are in terms of production and labor. Processes can be impacted by a 
        holiday on the horizon or coming back from a holiday.
        
        Parameters
        ----------
        min_date : datetime.date
            Minimum date to use to build cached calendar object.
        max_date : datetime.date
            Maximum date to use to build cached calendar object.
        countries : list, optional
            List of countries to get holidays for, by default ['US', 'CA']
        within_n_days : int, optional
            Flag if date is within n days of a holiday, by default 1
        
        Methods
        -------
        check_adjacent(check_date, country)

        Attributes
        ----------
        calendar : pd.DataFrame
            cache calendar of holidays created at init.
        """
        self._base_calendar = self._build_calendar(min_date, max_date, countries)
        self.calendar = self._build_out_calendar(countries, within_n_days)
    
    def _build_calendar(self, min_date, max_date, countries: list =['US']):
        date_list_ = pd.date_range(start=min_date, end=max_date).tolist()
        date_list = [x.date() for x in date_list_]
        cal = pd.DataFrame({'RefDate':date_list})

        for country in countries:
            _holidays = holidays.CountryHoliday(country)
            cal[country] = cal['RefDate'].apply(lambda x: x in _holidays)

        return cal

    def _adjacent_holiday(self, date, country: str = None, within_n_days: int = 1):
        ref_cal = self._base_calendar
        _min = (date - datetime.timedelta(days=int(within_n_days)))
        _max = (date + datetime.timedelta(days=int(within_n_days)))
        _range = pd.date_range(start=_min, end=_max).tolist()
        _range = [x.date() for x in _range]
        _cal = ref_cal[ref_cal['RefDate'].isin(_range)]

        if country is None:
            # any
            check_val = sum(_cal.iloc[:,1:].sum(axis=1))
        else:
            check_val = sum(_cal.loc[:, country])

        if check_val > 0:
            return True
        else:
            return False

    def _build_out_calendar(self, countries, within_n_days: int):
        df = self._base_calendar.copy()
        for country in countries:
            col_name = '{}_adjacent'.format(country)
            df[col_name] = np.vectorize(self._adjacent_holiday)(df['RefDate'],
                                                                country,
                                                                within_n_days)
        col_name = 'ANY_adjacent'
        df[col_name] = np.vectorize(self._adjacent_holiday)(df['RefDate'],
                                                            None,
                                                            within_n_days)
        df.set_index('RefDate', inplace=True)
        return df
        
    def check_adjacent(self, check_date, country:str = 'ANY'):
        """Check if a date is adjacent to a holiday of a country. Country must 
        be in list of countries at init.
        
        Parameters
        ----------
        check_date : datetime.date
            Date to check if a holiday is adjacent.
        country : str, optional
            Country to check from list of initial countries, by default 'ANY'
        
        Returns
        -------
        bool
            True if holiday is adjacent to date, else False.
        """
        check_col = country + '_adjacent'
        return self.calendar.loc[check_date, check_col]
