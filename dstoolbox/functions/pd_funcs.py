import re
import pandas as pd
import numpy as np

def fix_cols(df:pd.DataFrame, delim:str = '_', uppercase: bool = True):
    """
    Replace non-alpha-numeric characters in column names. Works inplace.

    Parameters
    ----------
    df : pd.DataFrame
        Dataframe to process.
    delim : str, optional
        New character to use inplace of removed characters. The default is '_'.
    uppercase : bool, optional
        Convert all the column names to UPPERCASE characters. The default is True.

    Returns
    -------
    None.

    """

    try:
        cols = df.columns
        new_cols = []
        for col in cols:
            # Replace % with Pct
            col = re.sub(r'(\%)', 'Pct', col)

            # Replace Δ with Delta
            col = re.sub(r'(\u0394)', 'Delta', col)

            # Fix non-alpha-numeric
            nc = re.sub('[^A-Za-z0-9]+', delim, col)
            if nc[len(nc)-1] == delim:
                nc = nc[:-1]
            
            # Convert to Uppercase
            if uppercase:
                nc = nc.upper()
                
            new_cols.append(nc)
        
        df.columns = new_cols
    except Exception as e:
        print(e)


def fix_date_cols(df: pd.DataFrame, re_flag: str = '(date)', 
                    fmt: str = '%Y-%m-%d', **kwargs):
    """
    Convert columns to date format based on column name. Useful when column 
    names have '(date)' or some other similar flag to denote that its a date.

    Parameters
    ----------
    df : pd.DataFrame
        Dataframe to process.
    
    re_flag : str, optional
        Regex expression to use to search the column name to check if it is a 
        date column. Default is '(date)'

    fmt : str
        Format that the str formated dates are in. Default is '%Y-%m-%d'
    
    **kwargs : kwargs, optional
        kwargs for pandas.to_datetime function

    Returns
    -------
    None. Processes in place.    

    """
    try:
        for col in df.columns:
            if re.search(re_flag, col, flags=re.IGNORECASE):
                if df[col].dtype != bool:
                    try:
                        df[col] = pd.to_datetime(df[col], format=fmt, **kwargs)
                    except Exception as e:
                        print('Date conversion failed on column: {}'.format(col))
                        print('Head of Failed Column:')
                        print(df[col].head())
                        
    except Exception as e:
        print(e)


def import_excel_regex_sheet_name(location:str, regex_sheet:str):
    """
    Import an excel tab based on REGEX for the sheet name. Useful when the name
    changes slightly due to poor practices.

    Parameters
    ----------
    location : str
        File location string.
    regex_sheet : str
        REGEX expression to match the sheet name.

    Returns
    -------
    pd.DataFrame

    """
    try:
        xl = pd.ExcelFile(location)
        name = [item for item in xl.sheet_names if re.search(regex_sheet, item)]
        if len(name) > 0:
            import_sheet_name = name[0]
            return pd.read_excel(location, sheet_name=import_sheet_name)
        else:
            print('No regex match to import.')
            return None
    except Exception as e:
        print(e)
        return None

def encode_cyclical_col(df, col:str, max_val=None, drop_original: bool = False):
    """
    Encode cyclical data such as Day of Week expressed as 1-7. The function
    will apply a SIN & COS transformation to the data so Sunday (7) and Monday (1)
    show as adjacent to each other instead of far apart.
    
    Parameters
    ----------
    df : pd.DataFrame
        DataFrame to modify.
    
    col : str
        Column name to apply transformation to.
    
    max_val : int or float, optional
        Max value to use. If left as none, it will use the max value in the
        column of the dataframe. Default is None.
    
    drop_original : bool, optional
        Drop the original column. Useful if you plan to drop later anyways.
        Default is False.
    
    Returns
    -------
    None -- applied in place.
    """

    try:
        if max_val is None:
            max_x = max(df[col])
        else:
            max_x = max_val
        
        df[col + '_sin'] = np.sin(2 * np.pi * df[col]/max_x)
        df[col + '_cos'] = np.cos(2 * np.pi * df[col]/max_x)
        
        if drop_original:
            df.drop(col, axis=1, inplace=True)
        
    except Exception as e:
        print('Error - Cyclical Data: {}'.format(e))


def generate_bin_labels(bins: list):
    """Generate a list of bin labels for use with pd.Cut()

    Note that Bins are a < x <= b

    Parameters
    ----------
    bins : list
        [description]

    Returns
    -------
    list
        List of labels with length n-1
    """    
    labels = []
    len_bins = len(bins)
    for i,b in enumerate(bins):
        if i < (len_bins - 2):
            next_b = bins[i+1]
            l = f'{b}-{next_b}'
            labels.append(l)
        elif i == (len_bins - 2):
            l = f'{b}+'
            labels.append(l)
        else:
            pass
        
    return labels