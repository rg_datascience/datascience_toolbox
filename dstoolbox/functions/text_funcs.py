import re

def multiple_replace(text: str, dict_: dict):
    """Use a dictionary to perform string replacement. Useful for normalizing
    free form text if you have a predfined dictionary of the various spellings
    for something. 

    Batteries not included. Provide a dictionary or replacements.

    Example:
        {'St. Joe': 'Saint Joseph'
        ,'Saint Joe': 'Saint Joseph'
        ,'Saint Joseph': 'Saint Joseph'}

        'St. Joe' --> 'Saint Joseph'

    Arguments:
        text {str} -- Text to be replaced.
        dict_ {dict} -- Dictionary of replacements.
    
    Returns:
        str -- Replaced text.
    """
    try:
        # Create a regular expression  from the dictionary keys
        regex = re.compile("^(%s)$" % "|".join(map(re.escape, dict_.keys())))
        # For each match, look-up corresponding value in dictionary
        return regex.sub(lambda mo: dict_[mo.string[mo.start():mo.end()]], text)
    except Exception as e:
        print(e)
        return text
