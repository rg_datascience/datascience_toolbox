import re
import math
from math import radians, cos, sin, asin, sqrt
from random import randint
from functools import lru_cache

'''-----------------------------------------------------------------------------
    ZIP CODE FUNCTIONS
-----------------------------------------------------------------------------'''

def flag_canada_zip(z: str):
    """Flag if a zipcode is a canadian zipcode.
    
    Parameters
    ----------
    z : str
        Zipcode to check
    
    Returns
    -------
    bool
    """
    z = str(z)
    rgx_ca = r'^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$'
    if re.search(rgx_ca, z):
        return True
    else:
        return False

def format_zip5(z):
    """Zipcodes on the east coast with leading zeros tend to get mangled on data
    import. This will correct the zipcode and return it as a 5 digit zipcode and
    as a string.
    
    Parameters
    ----------
    z : int, float, or str
        Zipcode that will be converted to string and formatting applied.
    
    Returns
    -------
    str
        properly formatted 5-digit zipcode
    """
    re_zip_gt5 = r'[0-9]{5}-[0-9]{0,4}'
    try:
        z = str(z)
        if len(z) == 3:
            return '00' + z
        elif len(z) == 4:
            return '0' + z
        elif re.search(re_zip_gt5, z):
            return z[:5]
        else:
            return z
    except Exception as e:
        print(e)
        return -1

'''-----------------------------------------------------------------------------
    CONVERSION FUNCTIONS
-----------------------------------------------------------------------------'''
def convert_dms_dd(dms: str):
    """Convert degree minute seconds format from PCMiler to decimal degrees.
    
    Parameters
    ----------
    dms : str
        PC Miler geocode "dddmmss[N|S],dddmmss[E|W]", ie. "0434443N,0814217W"
    """
    try:
        d = float(dms[:3])
        m = float(dms[3:5]) / 60
        s = float(dms[5:7]) / 3600
        direction = dms[-1]
        
        dd = d + m + s
        if direction in ['W', 'S']:
            dd *= -1
        return dd
    except:
        return -1  # standard error type for PC Miler.


def convert_meters_to_miles(m: float):
    """Convert Meters to Miles.
    
    Parameters
    ----------
    m : float
        Meters
    
    Returns
    -------
    float
        Miles
    """
    return m * 0.000621371

def convert_deg2rad(deg):
    """Convert degrees to radians.
    
    Parameters
    ----------
    deg : float
        Degrees to convert.
    
    Returns
    -------
    float
        radians
    """
    return (deg / 180) * math.pi


def convert_rad2deg(rad):
    """Convert radians to degrees.
    
    Parameters
    ----------
    rad : float
        Radians to convert.
    
    Returns
    -------
    float
        degrees
    """
    return rad * (180 / math.pi)

'''-----------------------------------------------------------------------------
    CARTESIAN FUNCTIONS
-----------------------------------------------------------------------------'''
def gen_point_from_bearing_distance(olat: float, olng: float, bearing: float, 
                                    distance_mi: float, rnd: int = 5):
    """Find a geographic point from an origin point, distance, and direction.

    Parameters
    ----------
    olat : float
        Origin latitude in degrees.
    olng : float
        Origin longitude in degrees.
    bearing : float
        Bearing in degrees (0 to 360)
    distance_mi : float
        Distance in miles away from the origin to return for the new point.
    rnd : int, optional
        Beyond 5 is dumb, by default 5

    Returns
    -------
    tuple
        Latitude and Longitude of new point in degrees.
    """
    R = 3956
    d = distance_mi
    rlat = math.radians(olat)
    rlng = math.radians(olng)
    rbearing = math.radians(bearing)
    
    rlat2 = math.asin(math.sin(rlat)*math.cos(d/R) +
             math.cos(rlat)*math.sin(d/R)*math.cos(rbearing))
    
    rlng2 = rlng + math.atan2(math.sin(rbearing) * math.sin(d/R) * math.cos(rlat),
                     math.cos(d/R)-math.sin(rlat) * math.sin(rlat2))
    lat2 = round(math.degrees(rlat2),rnd)
    lng2 = round(math.degrees(rlng2),rnd)
    return lat2, lng2


def get_bearing(lat1: float, lon1: float, lat2: float, lon2: float, 
                rnd: int = 4, mode: int = 360):
    """Calculate the bearing given an origin point and secondary point.
    
    Parameters
    ----------
    lat1 : float
        Origin Latitude
    lon1 : float
        Origin Longitude
    lat2 : float
        Destination Latitude
    lon2 : float
        Destination Longitude
    rnd : int, optional
        Number of decimal places to round to, by default 4
    mode : int, optional
        360 returns 0 to 360 degrees, 180 returns -180 to 180 degrees, 
            by default 360
    
    Returns
    -------
    float
        bearing
    """

    try:
        lat1 = convert_deg2rad(lat1)
        lon1 = convert_deg2rad(lon1)
        lat2 = convert_deg2rad(lat2)
        lon2 = convert_deg2rad(lon2)

        bearing_rad = math.atan2(math.sin(lon2 - lon1) * math.cos(lat2),
                                 math.cos(lat1) * math.sin(lat2) - math.sin(lat1)\
                                    * math.cos(lat2) * math.cos(lon2 - lon1))

        bearing = round(convert_rad2deg(bearing_rad), rnd)

        if mode == 180:
            return bearing

        elif mode == 360:
            if bearing < 0:
                new_bearing = 360 + bearing
            else:
                new_bearing = bearing

            return new_bearing

    except Exception as e:
        print(e)
    return -1


def haversine(lat1: float, lon1: float, lat2: float, lon2: float,
              rnd: int = 1, apply_sqrt2: bool=False):
    """Calculate the great circle distance between 2 points on the globe.
    Commonly referred to 'as the crow flies'. Good for estimated relative
    distances between points and a lot faster than a distance API but lacks
    the accuracy of real road miles api (like Google Maps, or PC Miler.)
    
    Parameters
    ----------
    lat1 : float
        Latitude of point 1.
    lon1 : float
        Longitude of point 1.
    lat2 : float
        Latitude of point 2.
    lon2 : float
        Longitude of point 2.
    rnd : int, optional
        Number of digits to round to, defaults to 1.
    apply_sqrt2 : bool, optional - default False
        Multiply the result by sqrt of 2 usually gives a good assessment of actual travel distance
    
    Returns
    -------
    float
        distance in miles
    """    
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    r = 3956

    result = round(c * r, rnd)

    if apply_sqrt2:
        result = round(result * sqrt(2),rnd)
    
    return result

'''-----------------------------------------------------------------------------
    MISC GEO FUNCTIONS
-----------------------------------------------------------------------------'''
common_ca_province_remaps = {
    'PQ': 'QC',
    'QU': 'QC',
    'NF': 'NL'
}


def get_canada_province_codes(iso_only:bool = True):
    """
    ISO 3166-2:CA https://en.wikipedia.org/wiki/Canadian_postal_abbreviations_for_provinces_and_territories
    
    Parameters
    ----------
    iso_only : bool, optional
        [description], by default True
    
    Returns
    -------
    [type]
        [description]
    """
    provinces = ['AB', 'BC', 'MB', 'NB', 'NL', 'NF', 'NS', 'ON', 'PE'
                ,'QC', 'PQ', 'SK', 'NT', 'NU', 'YK']

    iso_provinces = ['AB', 'BC', 'MB', 'NB', 'NL', 'NS', 'NT', 'NU', 'ON', 'PE'
                    ,'QC', 'SK', 'YT']

    if iso_only:
        return iso_provinces
    else:
        return provinces
    
def remap_canada_province_codes_iso(code:str, dictionary:dict = None):
    if dictionary is None:
        d = common_ca_province_remaps
    else:
        d = dictionary
    if code in d.keys():
        return d[code]
    else:
        return code

def get_us_state_codes(return_type:str='list', contiguous:bool=True):
    """Quickly get a list or comma delimited string of the US state codes,
    optionally excluding HI and AK.
    
    Parameters
    ----------
    return_type : str, optional
        Return a 'list' or a comma delimited string, by default 'list'. 
            Options ['list', 'str']
    contiguous : bool, optional
        If True, will exclude HI and AK, by default True.
    
    Returns
    -------
    list (or str)
    """
    codes = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
              "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
              "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
              "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
              "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

    if contiguous:
        codes = [x for x in codes if x not in ['HI', 'AK']]

    if return_type == 'list':
        return codes
    elif return_type == 'str':
        return ','.join(codes)
    else:
        return codes


def validate_us_yx(*args):
    """Check if latitude, longitude are inside the contiguous US based on a 
    rough estimation. Can pass lat,lng as a tuple or a single comma delimeted
    string.
    
    Returns
    -------
    bool
    """
    _regex_geo = r'^([-+]?([1-8]?\d(\.\d+)?|90(\.0+)?))(\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?))$'
    _us_top = 49.3457868  # north lat
    _us_left = -124.7844079  # west long
    _us_right = -66.9513812  # east long
    _us_bottom = 24.7433195  # south lat

    def get_res(lat, lng):
        if re.search(_regex_geo, str(lat)) and re.search(_regex_geo, str(lng)):
            if _us_bottom <= lat <= _us_top and _us_left <= lng <= _us_right:
                return True
            else:
                return False
        else:
            return False

    if len(args) == 1:
        yx = args[0]
        if type(yx) is tuple:
            lat, lng = yx[0], yx[1]
        elif type(yx) is str:
            yx = yx.replace(' ', '')
            lat, lng = yx.split(',')
            lat, lng = float(lat), float(lng)
        return get_res(lat, lng)

    elif len(args) == 2:
        lat = args[0]
        lng = args[1]

        if type(lat) is not float:
            lat = float(lat)
        if type(lng) is not float:
            lng = float(lng)
        return get_res(lat, lng)

    else:
        print('_validate_us_yx: Invalid number of arguments: %d' % len(args))
        return False

@lru_cache(maxsize=1000)
def validate_geocode(scope:str, *args):
    """Check if latitude and longitude are valid for the given scope.

    Parameters
    ----------
    scope : str
        - 'US_wTerrirtories' is all of US and US territories
        - 'US48' is the contiguous US
        - 'CA' is Canada
        - 'MX' is Mexico

    Returns
    -------
    bool
        
    """
    _regex_geo = r'^([-+]?([1-8]?\d(\.\d+)?|90(\.0+)?))(\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?))$'
    _scope_dict = {
        'US_wTerrirtories':{
            'min_latitude': 5.87,
            'max_latitude': 71.39,
            'min_longitude': -180,
            'max_longitude': -66.95138
        },
        'US48':{
            'min_latitude': 24.7433195,
            'max_latitude': 49.3457868,
            'min_longitude': -124.7844079,
            'max_longitude': -66.951312
        },
        'CA': {
            'min_latitude': 41.71,
            'max_latitude': 83.21,
            'min_longitude': -141,
            'max_longitude': -52.62
        },
        'MX': {
            'min_latitude': 14.54,
            'max_latitude': 32.72,
            'min_longitude': -118.37,
            'max_longitude': -86.71
        }
    }
    
    def get_res(lat, lng, scope:dict):
        bottom = scope['min_latitude']
        top = scope['max_latitude']
        left = scope['min_longitude']
        right = scope['max_longitude']
        
        if re.search(_regex_geo, str(lat)) and re.search(_regex_geo, str(lng)):
            if bottom <= lat <= top and left <= lng <= right:
                return True
            else:
                return False
        else:
            return False
    
    scope_ = _scope_dict[scope]
    
    if len(args) == 1:
        yx = args[0]
        if type(yx) is tuple:
            lat, lng = yx[0], yx[1]
        elif type(yx) is str:
            yx = yx.replace(' ', '')
            lat, lng = yx.split(',')
            lat, lng = float(lat), float(lng)
        return get_res(lat, lng, scope_)

    elif len(args) == 2:
        lat = args[0]
        lng = args[1]

        if type(lat) is not float:
            lat = float(lat)
        if type(lng) is not float:
            lng = float(lng)
        return get_res(lat, lng, scope_)

    else:
        print('_validate_us_yx: Invalid number of arguments: %d' % len(args))
        return False
