"""
TODO: Create separate timer class(?) (Multiple timers or per Printr?)
"""
from math import floor, ceil
from time import time, strftime
from datetime import timedelta

class Printr():
    def __init__(self, max_width: int = 79):
        self._max_width = max_width
        self._time_start = 0
        self._time_stop = 0
        self._timer_running = False
        self._last_time_elapsed = 0
    
    @property
    def max_width(self):
        return self._max_width
    

    def set_max_width(self, value):
        if value > 0:
            self._max_width = value

    @property
    def time_start(self):
        return self._time_start
    

    def start_timer(self):
        self._time_start = time()
        self._timer_running = True

    @property
    def time_stop(self):
        return self._time_stop
    
    def stop_timer(self):
        self._time_stop = time()
        self._timer_running = False
        self.set_time_elapsed(self._time_start, self._time_stop)
    
    @property
    def last_time_elapsed(self):
        lte = self._last_time_elapsed
        if lte > 0:
            return lte
        else:
            return 0

    def set_time_elapsed(self, start, stop):
        self._last_time_elapsed = stop - start

    def _get_filler_sizes(self, s):
        
        len_fill = self.max_width - (len(s) + 2)
        _len_fill = len_fill / 2
        if len_fill % 2 == 0:
            len_fill_left = int(_len_fill)
            len_fill_right = int(_len_fill)
        else:
            len_fill_left = int(floor(_len_fill))
            len_fill_right = int(ceil(_len_fill))
        return len_fill_left, len_fill_right

    def _gen_print(self, s:str, decorator:str, pad_left:int, pad_right:int):
        return decorator * pad_left + ' ' + s + ' ' + decorator * pad_right

    def p(self, *args, **kwargs):
        """
        Alias function for printr.
        """
        self.printr(*args, **kwargs)

    def printr(self, s: str, decorator: str = '-', enclose: bool = False,
                enclosure_decorator: str = None, start_timer: bool = False, 
                stop_timer: bool = False):

        if enclosure_decorator is None:
            enclosure_decorator = decorator

        #  Handle Timer
        if start_timer:
            self.start_timer()
        elif stop_timer:
            self.stop_timer()
        
        #  Enclose Top
        if enclose:
            print(enclosure_decorator * self.max_width)

        if stop_timer and self.last_time_elapsed > 0:
            seconds = timedelta(seconds = self.last_time_elapsed)
            time_elapsed = str(seconds)[:-3]
            s = s + ' ({})'.format(time_elapsed)
        
        #  Determine Side Filler Sizes
        len_fill_left, len_fill_right = self._get_filler_sizes(s)
        
        print(self._gen_print(s, decorator, len_fill_left, len_fill_right))

        #  Enclose Bottom
        if enclose:
            print(enclosure_decorator * self.max_width)
