# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 08:56:21 2017

@author: Reece Althoff
@title: PreGrouper
@status: Beta
@version: 1.0.5
"""

from random import randint
import random, string
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from tqdm import tqdm
import datetime as dt

class SampleDataGenerator(object):
       
    def __init__(self, sample_size:int=100, sample_val_min:int=500, 
            sample_val_max:int=10000):
        self.sample_size = sample_size
        self.sample_val_min = sample_val_min
        self.sample_val_max = sample_val_max
        self.GenSampleData()
    
    def GenSampleData(self, rs_size:tuple=(400,600), sample_val_min:int=500, 
            sample_val_max:int=10000):
        '''
        Generate a sample data set.
        
        Arguments:
            rs_size (tuple): Min and max value for a range in which to generate
            a random number of records. (Default: 400,600)
            
            sample_val_min (int): Minimum for range of values to randomly generate (Default: 500)
            
            sample_val_max (int): Maximum for range of values to randomly generate (Default: 10000)
            
        Reutrns:
            dataframe with two columns ['uid', 'value']
        '''
        
        rs_min, rs_max = rs_size
        uids, vals = self._GenSampleDataSet(sample_size=randint(rs_min, rs_max), 
                sample_val_min=sample_val_min, sample_val_max=sample_val_max)
        df = pd.DataFrame({
                'uid':uids,
                'value':vals
                })
        

        self.data = df
    
    ''' Generate Sample Data '''
    def _GenSampleDataSet(self, sample_size:int=None, 
            sample_val_min:int=None, sample_val_max:int=None):
        '''
        Generate a sample data set
        
        Arguments:
            sample_size (int):      record quantity to return
            
            sample_val_min (int):   floor of the random values
            
            sample_val_max (int):   ceiling of te random values
    
        
        Returns:
            uids (list):    list of unique identifiers
            
            values (list):  list of the random values
            
        '''
        if sample_size is None:
            sample_size = self.sample_size
        if sample_val_max is None:
            sample_val_max = self.sample_val_max
        if sample_val_min is None:
            sample_val_min = self.sample_val_min

        
        uid_len = self._DetermineUID_Length(sample_size)
        uids = []
        values = []
        for i in range(sample_size):
            key = self._GetUIDkey(uid_len, uids)
            uids.append(key)
            values.append(randint(sample_val_min, sample_val_max))
        return uids, values
    
    def _DetermineUID_Length(self, data_len:int):
        '''
        Calculate length of key needed to support the dataset without risk of 
        duplication or running out of combinations while keeping it as small
        as possible.
        
        Arguments:
            data_len (int): Size of the dataset.
        
        Returns:
            int
        
        '''
        
        l = data_len
        n = []
        for i in range(1,6):
            if l < len(string.ascii_letters+string.digits) ** i:
                n.append(i)
        return min(n)+1
    
    
    def _GenUIDkey(self, length):
        '''
        Generate a random UID Key.
        
        Arguments:
            length (int): Size of key to return.
            
        Returns:
            string
        '''
        uid = ''.join(random.choice(string.ascii_letters+string.digits) for _ in range(length))
        return uid
    
    def _GetUIDkey(self, length, vals):
        '''
            Get a UID Key
            
            Arguments:
                length (int): Size of key.
                
                vals (list): List of current UIDs to prevent duplication.
            
            Returns:
                string
        '''
        uid = self._GenUIDkey(length)
        while uid in vals:
            uid = self._GenUIDkey(length)
        return uid
    
class PreGrouper(object):
    '''
    The PreGrouper (a variation of the Bin Packing Algorithm) can take items 
    that need to be combined and group them accordingly by value to maximize 
    the value per bin and minimize the count of bins that are created for 
    grouping the items. 
    
    Main Functions:
        AdvancedPreGroup:
            Takes the data, shuffles the order and splits it. Then groups the
            data and backfills to minimize bin count and maximize total value
            per bin. The final bin is usually on par with utilization as the
            other bins created. Overall might be slightly lower than BinPacking.
        
        BinPacking:
            Uses the Bin Packing Heuristics algorithm to group items. About 100x
            faster than AdvancedPreGroup but can return a less optimal result 
            in some (but not all) cases. Final bin has a high tendancy of being
            far below the max value for the bin.
        
        run_benchmarks:
            Runs benchmarks of the various methods to compare performance.
        
        run_example:
            Takes in a dataset and runs the optimizers based on mode argument passed.
    
    '''
    
    
    ''' Define Constraints '''
    
    
    def __init__(self, group_val_max:int=45000, i_pre_grp_max:int=45000):        
        self.group_val_max = group_val_max
        self.i_pre_grp_max = i_pre_grp_max
        
    
    def run_benchmarks(self, data, iterations:int=100, split:float=0.2, 
            pre_grp_max:int=None, mode:int=1):
        '''
        Benchmark Performance of the various grouping methods in the module.
        
        Arguments:
            data (dataframe): 2 column DF [uid, value]
            
            iterations (int): Number of trials to run. (Default: 1000)
            
            split (float): Percentage of records to randomly set aside for backfill.
            
            pre_grp_max (int): Maximum value per bin/group.
            
            mode (int):
                1: Simple Randomized Pre Group operation
                
                2: Advanced PreGrouping with advanced backfill and returns
                the best solution from a defined number of sub-iterations
                (default is 10)
                
                3: Bin Packing Hueristic. up to 100x faster than option 2 but 
                the speed comes at the cost of performance (est 5-10% impact
                overall, and last bin can vary widely)
        
        Returns:
            Nothing. Console output of results of the benchmarks.
        '''

        if pre_grp_max is None:
            pre_grp_max = self.i_pre_grp_max

        performance = []
        p_last = []
        
        def _agg_results(df_opt):
            if 'group' in df_opt.columns:
                df_agg = df_opt.groupby(by='group').agg({'uid':'count', 'value':'sum'})\
                    .rename(columns={'uid':'item_count', 'value':'total_value'})
                df_agg['Performance'] = df_agg['total_value'].apply(lambda x: x/pre_grp_max)  # Ensure pre_grp_max is defined
            else:
                # Since there's no 'group', aggregate without grouping
                agg_data = {
                    'item_count': len(df_opt),
                    'total_value': df_opt['value'].sum() if 'value' in df_opt.columns else 0
                }
                # Create a DataFrame with a single row of aggregated data
                df_agg = pd.DataFrame([agg_data])
                # Calculate 'Performance' assuming a relevant 'pre_grp_max' value or set a default
                df_agg['Performance'] = df_agg['total_value'].apply(lambda x: x/pre_grp_max if 'pre_grp_max' in locals() else 0)
                
                # Optionally, add a placeholder for 'group' to match format
                df_agg.insert(0, 'group', 'all')

            return df_agg

        for _ in tqdm(range(iterations)):    
            df = data
            if mode == 1:
                '''
                Simple Randomized Pregroup
                '''
                dfPg = self._PreGroup(df, pre_grp_max=pre_grp_max, split=split)
                dfPg_agg = _agg_results(dfPg)

                P = np.average(dfPg_agg['Performance'])
                performance.append(P)
                p_last.append(dfPg_agg['Performance'][dfPg_agg.index[-1]])

            elif mode == 2:
                '''
                Advanced PreGrouping with backfill
                '''
                try:
                    dfOpt, df_ug = self.AdvancedPreGroup(data, init_split=split,
                            pre_group_max=pre_grp_max)
                    if len(dfOpt) > 0:
                        dfPg_agg = _agg_results(dfOpt)

                        P = np.average(dfPg_agg['Performance'])
                        performance.append(P)
                        p_min = min(dfPg_agg['Performance'])
                        p_last.append(p_min)
                except Exception as e:
                    print('Advanced Pregrouping Error')
                    print(e)
                    pass

            elif mode == 3:
                '''
                Standard Bin Packing Algorithm
                '''
                dfOpt = self.BinPacking(data, bin_max=self.group_val_max)
                #P = np.average(dfOpt['Perf'])
                #performance.append(P)
                #p_min = min(dfOpt['Perf'])
                #p_last.append(p_min)
                dfPg_agg = _agg_results(dfOpt)
                P = np.average(dfPg_agg['Performance'])
                performance.append(P)
                p_min = min(dfPg_agg['Performance'])
                p_last.append(p_min)
            
                
        self._PrintBenchmarkResults(performance, 'OVERALL PERFORMANCE', 
            pre_grp_max=pre_grp_max)
        self._PrintBenchmarkResults(p_last, 'WORST GROUP PERFORMANCE', 
            pre_grp_max=pre_grp_max)
    
    
    def _PrintBenchmarkResults(self, performance, title, pre_grp_max:int, 
            grp_total_max:int=None):
        
        if grp_total_max is None:
            grp_total_max = self.group_val_max

        self._PrintHeader(title)
        _format = '{0:.4f}'
        _formatW = '{0:,.0f}'
        print('Pre-Group Max:', _formatW.format(pre_grp_max))
        print('Total Group Max:', _formatW.format(grp_total_max), '\n')
        P_ = np.average(performance)
        P_std = np.std(performance)
        P_min = min(performance)
        P_max = max(performance)
        print("P Avg:", _format.format(P_), " | P Avg(lbs):", 
            _formatW.format(P_ * grp_total_max))
        print('P Std:', _format.format(P_std), " | P Std(lbs):", 
            _formatW.format(P_std * grp_total_max))
        print('P Min:', _format.format(P_min), " | P Min(lbs):", 
            _formatW.format(P_min * grp_total_max))
        print('P Max:', _format.format(P_max), " | P Max(lbs):", 
            _formatW.format((P_max * grp_total_max)))
        print('P Rng:', _format.format(P_max - P_min), " | P Rng(lbs):", 
            _formatW.format((P_max - P_min) * grp_total_max),'\n')
    
    
    def _PrintHeader(self, s, buffer:int=9, sym:str='='):
        '''
        Print a header in the console
        
        Arguments:
            s (string): Text for the header
            
            buffer (int): Size of padding to left and right of the text (Default: 9)
            
            sym (str): Symbol to use to border the header string
            
        Returns:
            None
        '''
        
        _len = len(s) + (2*buffer)
        print(sym * _len)
        print(sym * (buffer-1), s, sym *(buffer-1))
        print(sym * _len)
    

    
    def _PreGroup(self, df, init_group:int=1, pre_grp_max:int=None, 
            split:float=0.2):
        '''
        Initial PreGroup operation. Source data is split and grouped. Ungrouped
        subset is used to backfill in subsequent operations to maximize utilization
        and minimize group count.
        
        Arguments:
            df (dataframe): 2 column DF [uid, value]
            
            init_group (int): Initial Group ID number to use. (Default: 1)
            
            pre_grp_max (int): Max value for group in the initial pre-group. 
            Should be less than the absolute max so it can be backfilled.
            
            split (float): Train/Test Split value, percentage of data to set 
            aside for backfill operations. (Default: 0.20)
            
        Returns:
            df (dataframe): Dataframe same as the input but with a column added
            with the group ID number generated by the PreGroup operation.
        
        '''
        if pre_grp_max is None:
            i_pre_grp_max = self.i_pre_grp_max
        try:
            if len(df) == 1:
                df['group'] = None
                return df

            if not split > 0:
                split = 0.2
            grpd, nogrp = train_test_split(df, test_size=split)
            
            vals = []
            group = init_group
            uids = []
            groupids = []
            
            for index, row in grpd.iterrows():
                if (sum(vals) + row['value']) <= pre_grp_max:
                    uids.append(row['uid'])
                    groupids.append(group)
                    vals.append(row['value'])
                else:
                    group += 1
                    vals = []
                    uids.append(row['uid'])
                    groupids.append(group)
                    vals.append(row['value'])
                
            dfx = pd.DataFrame({'uid':uids, 'group':groupids}).set_index('uid')
            df = pd.concat([grpd, nogrp])
            df = pd.merge(df, dfx, how='left', left_on='uid', right_index=True)
            
            return df
        except Exception as e:
            print(e)
            print('DF Size: {} | Split: {}'.format(len(df), split))
    
    
    def _ProcessPreGrouped(self, df):
        '''
        Take the item level dataframe from _PreGroup and create a group level 
        dataframe for further processing in _FinalizeGrouping.
        
        Arguments:
            df (dataframe): Resulting dataframe from the _PreGroup operation.
            
        Returns:
            d_groups (dict): dictionary of the groups
            
            df_ug (dataframe): subset of df where no group was assigned.
        '''
        df = df.fillna('no_group')
        len_df = len(df[df['group'].isin(['no_group'])])

        if len_df > 0:
            dfx = df[~(df.group == 'no_group')]
            df_ug = df[(df.group == 'no_group')]
        
        else:
            dfx = df
            df_ug = pd.DataFrame(columns=list(df))
        
        groups = np.unique(dfx['group']).tolist()
        uids = []
        wts = []
        for group in groups:
            _dfx = dfx[(dfx.group==group)]
            _uids = _dfx.uid.tolist()
            uids.append(_uids)
            _wts = _dfx.value.tolist()
            wts.append(_wts)
        
        d_groups = {}
    
        for i in range(len(groups)):
            gid = groups[i]
            d_groups[gid] = {}
            d_groups[gid]['uids'] = uids[i]
            d_groups[gid]['weight'] = wts[i]
        
        return d_groups, df_ug
    
    def _FinalizeGrouping(self, grouped:dict, ungrouped, maxwgt:int=45000):
        '''
        Backfills grouped bins with ungrouped items until the bins reach their
        maxwgt value then returns a dataframes for grouped and ungrouped.
        
        Arguments:
            grouped (dict): Dictionary result from _ProcessPreGrouped for the 
            grouped items
            
            ungrouped (dataframe): ungrouped items from _PreGroup operation
            
            maxwgt (int): Max value for each bin that is not to be exceeded 
            (Default: 45,000)
        
        Returns:
            grouped (dataframe)
            
            ungrouped [ug] (dataframe)
        '''
        
        
#        if ungrouped == None:
#            ug = pd.DataFrame(columns=['uid', 'value', 'group'])
#        else:
        ug = ungrouped
            
        for k,v in grouped.items():
            _wgts = v['weight']
            _uids = v['uids']
            
            _wgt = sum(_wgts)
            _wgtDelta = maxwgt - _wgt
            
            _ug = ug[(ug['value'] <= _wgtDelta) & (ug['group'] == 'no_group')]
            
            _uidx = []
            _wgtx = []
            
            for index, row in _ug.iterrows():
                if row['value'] <= _wgtDelta:
                    _uidx.append(row['uid'])
                    _wgtx.append(row['value'])
                    _wgtDelta -= row['value']
                    ug.at[index, 'group'] =  k
            
            _wgtY = _wgts + _wgtx
            _uidY = _uids + _uidx
            
            grouped[k]['weight'] = _wgtY
            grouped[k]['uids'] = _uidY
            
        ug = ug[(ug['group']=='no_group')]
        df = pd.DataFrame.from_dict(grouped, 'index')
        
        return df, ug
    
    def _GetNewSplit(self, init_split:float, splits:list, rnd:int=2, 
            range_limit:float=0.1, delta_min:float=0.025):
        '''
        Randomly generate a value for the Train/Test Split operation.
        
        Arguments:
            init_split (float): Initial split value (ex: 0.30)
            
            splits (list): list of previous splits to avoid repitition
            
            rnd (int): Number of decimal places to round to
            
            range_limit (float): Range of how far plus/minus from the init_split 
            the value can be. (Default: 0.1)
            
            delta_min (float): Minimum change the new value has to be from the 
            old value (Default: 0.025)
        
        Returns:
            new_split (float)
        '''
        
        if abs(range_limit) > abs(init_split):
            range_limit = abs(init_split)
            
        
        lim_l = init_split - range_limit
        lim_u = init_split + range_limit
        new_split = round(random.uniform(lim_l, lim_u),rnd)
        while True:
            delta_ok = abs(init_split - new_split)>delta_min
            list_check_ok = new_split not in splits
            
            if delta_ok == False or list_check_ok == False:    
                new_split = round(random.uniform(lim_l, lim_u), rnd)
            else:
                break
        return new_split
    
    def AdvancedPreGroup(self, data, iteration_max:int=10, 
            reconcile_iter_max:int=10, pre_group_max:int=35000, 
            group_max:int=None, init_split:float=0.3, 
            perf_goal:float=0.98, silent_mode:bool=True):
        '''
        Randomized sorting and selection.
        
        Arguments:    
            data (dataframe)    : 2 column DF [uid, value]
            
            iteration_max (int) : max iterations to find optimum mix (default = 100)
            
            pre_group_max (int) : max value in the pre-group (default = 35,000)
            
            group_max (int)     : max value for final groups
            
            init_split (float)  : test / train split for pre-grouping (default = 0.3)
            
            perf_goal (float)   : overall performance goal to attain before returning results (default = 0.98)
            
        Returns:
                res (dataframe): grouping results
                
                ug (dataframe): ungrouped
        '''
        if group_max is None:
            group_max = self.group_val_max
        
        def pregroup_ops(data, split, pre_grp_max:int=pre_group_max, init_group:int=1):
            dfPg = self._PreGroup(data, pre_grp_max=pre_group_max, split=split, init_group=init_group)
            if dfPg is not None:
                d_groups, dfUg = self._ProcessPreGrouped(dfPg)
                df, ug = self._FinalizeGrouping(d_groups, dfUg, maxwgt=group_max)
            try:
                df['total_wgt'] = df['weight'].apply(lambda x: sum(x))
                df['Performance'] = df['total_wgt'].apply(lambda x: x/group_max)
                return df, ug
            except:
                return None, None
    
        
        def iterate(split:float):
            _iter = 0
            _ugcnt = 1
            _perf = 0.0
            
            if silent_mode == False:
                _desc = 'Processing (%d)' % len(data)
                pbar = tqdm(desc=_desc)
                
            while _iter < iteration_max:  
                
                if _ugcnt == 0 and _perf >= perf_goal:
                    break
                else:
                    df1, ug = pregroup_ops(data, split)
                    ''' first pass on ungrouped '''
                    if ug is not None:
                        if len(ug) > 0:
                            df2, ug = pregroup_ops(ug[ug.columns[:-1]], \
                                init_group=(len(df1)+1), split=split)
                            df1 = pd.concat([df1, df2])
                            
                            ''' final pass on ungrouped '''
                            if ug is not None:
                                if len(ug)>0:
                                    df3, ug = pregroup_ops(ug[ug.columns[:-1]],\
                                        init_group=(len(df1)+1), split=0.0,
                                        pre_grp_max=group_max)
                                    df1 = pd.concat([df1, df3])
                    
                    try:
                        _ugcnt = len(ug)
                    except Exception as e:
                        _ugcnt = 0
                        print("apg.iterate", e)
                        
                    _perf = np.average(df1['Performance'])
                    _iter += 1
                    
                    if silent_mode == False:
                        pbar.update(1)
            
            if silent_mode == False:
                print('Performance:', _perf, '| UG Count:', _ugcnt,
                    '| Iteration:', _iter)
                if _ugcnt > 0:
                    print('Not Optimal. Ungrouped:', _ugcnt)
                
            return df1, ug, _ugcnt, _perf
        
        def GetBestSolution(perfs, results, ugs):
            i = perfs.index(max(perfs))
            df = results[i]
            ug = ugs[i]
            return df, ug
            
        res, ug, ugcnt, perf = iterate(split=init_split)
        
        iterations=[]
        perfs = []
        results = []
        ugs = []
        
        reconcile_iter = 0
        splits = [init_split]
        
        while reconcile_iter <= reconcile_iter_max:
            iterations.append(reconcile_iter)
            perfs.append(perf)
            results.append(res)
            ugs.append(ug)
            if ugcnt == 0 and perf > perf_goal:
                
                break
            else:       
                new_split = self._GetNewSplit(init_split, splits, rnd=3)
                splits.append(new_split)
                if silent_mode == False:
                    print('Shuffling the deck {}... New Split Val: {}'.format\
                        (reconcile_iter+1, new_split))
                res, ug, ugcnt, perf = iterate(split=new_split)
                reconcile_iter += 1
        res, ug = GetBestSolution(perfs, results, ugs)
        if silent_mode == False:
            uglen = 0 if ug is None else len(ug)
            _perf = np.average(res['Perf'])*100
            _wgperf = min(res['Perf'])*100
            print('Bin Count: {} | Un-Grouped: {} | Overall Performance: {} | Worst Group Performance: {}'.format(len(res), uglen, '{0:.4f}%'.format(_perf), '{0:.4f}%'.format(_wgperf)))
        return res, ug
    
    def BinPacking(self, data, col_uid:str='uid', col_val:str='value',
                    bin_max:int=None):
        '''
        Implementation of the Bin Packing Hueristics algorithm. About 100x
        faster than the PreGroup algorithm for a small cost in performance.
        
        Arguments:    
            data (dataframe)    : 2 column DF [uid, value]
            
            group_max (int)     : max value for final groups
            
            
        Returns:
                DataFrame with results
        '''
        if bin_max is None:
            bin_max = self.group_val_max

        vals = []
        group = 0
        uids = []
        groupids = []
        
        data.sort_values(by=[col_val], ascending=False)
        
        for index, row in tqdm(data.iterrows()):
            foundgroup=False
            if group == 0: 
                stoprange=0
            else:
                stoprange=group
                
            for i in range(0, stoprange):
                if (sum(vals[i]) + row[col_val]) <= bin_max:
                    uids[i].append(row[col_uid])
                    groupids[i].append(i+1)
                    vals[i].append(row[col_val])
                    foundgroup=True
                    break
            if not foundgroup:
                vals.append([])
                uids.append([])
                groupids.append([])
                uids[group].append(row[col_uid])
                groupids[group].append(group+1)
                vals[group].append(row[col_val])
                group += 1

        dfx = pd.DataFrame({'uid':np.hstack(uids), 
                'group':np.hstack(groupids)}).set_index('uid')
        df = pd.merge(data, dfx, how='left', left_on='uid', right_index=True)

        return df

        
    
    def run_example(self, data, run_benchmarks:bool=True, 
                    return_example_set:bool=True, example_mode:int=1, 
                    silent_mode=False):
        '''
        Arguments:
            data (dataframe): 2 column DF [uid, value]
            run_benchmarks (bool):  
                Print benchmarking results to console
            
            return_example_set (bool): 
                Return grouped example dataset based on a random sample
            
            example_mode (int):
                1 = Basic pre-group\n
                2 = Advanced pre-group\n
                3 = Advanced Pre-Group Vs Bin Sorting Algorithm\n
                    (returns tuple of dataframes, first one is the APG, 
                    second is the Bin)
            
        Returns:
            DataFrame with results
                
            
        '''

        df = data
            
            
        if run_benchmarks == True:
            self._PrintHeader('Method 1', buffer=25)
            self.run_benchmarks(df, iterations=25)
        
            self._PrintHeader('Method 2', buffer=25)
            self.run_benchmarks(df, iterations=25, mode=2, pre_grp_max=35000)
            
            self._PrintHeader('Method 3', buffer=25)
            self.run_benchmarks(df, iterations=25, mode=3)
            
        if return_example_set == True:
            if example_mode == 1:
                dfx = self._PreGroup(df)
            elif example_mode == 2:
                dfx = self.AdvancedPreGroup(df, silent_mode=silent_mode)
            elif example_mode == 3:
                test_data = df
                print('=' * 30)
                print('---- APG ----')
                t1 = dt.datetime.now()
                apg_algo = self.AdvancedPreGroup(test_data, 
                    silent_mode=silent_mode)[0]
                t2 = dt.datetime.now()
                print('Advanced PG Algorithm Time: {}'.format(t2-t1))
                print('-' * 30)
                print('---- BSA ----')
                t1 = dt.datetime.now()
                res_bins = self.BinPacking(test_data)
                r2 = self._ProcessPreGrouped(res_bins)[0]
                df = pd.DataFrame.from_dict(r2, 'index')
                df['total_wgt'] = df['weight'].apply(lambda x: sum(x))
                df['Perf'] = df['total_wgt'].apply(lambda x: x/self.group_val_max)
                print('Bin Count:', len(df),' | Bin Avg Performance:', '{0:.4f}%'\
                    .format(np.average(df['Perf'])*100), ' | Worst Group Perf:',
                     '{0:.4f}%'.format(np.min(df['Perf'])*100))
                t2 = dt.datetime.now()
                print('Bin Algorithm Time: {}'.format(t2-t1))
                dfx = apg_algo, df
            else:
                print('Unknown Mode')
                dfx = pd.DataFrame()
            return dfx
