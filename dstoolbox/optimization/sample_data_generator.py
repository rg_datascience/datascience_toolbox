from random import randint
import random, string
#import numpy as np
import pandas as pd
#from sklearn.model_selection import train_test_split
#from tqdm import tqdm
#import datetime as dt

class SampleDataGenerator(object):
       
    def __init__(self, sample_size:int=100, sample_val_min:int=500, 
            sample_val_max:int=10000):
        self.sample_size = sample_size
        self.sample_val_min = sample_val_min
        self.sample_val_max = sample_val_max
        self.GenSampleData()
    
    def GenSampleData(self, rs_size:tuple=(400,600), sample_val_min:int=500, 
            sample_val_max:int=10000):
        '''
        Generate a sample data set.
        
        Arguments:
            rs_size (tuple): Min and max value for a range in which to generate
            a random number of records. (Default: 400,600)
            
            sample_val_min (int): Minimum for range of values to randomly generate (Default: 500)
            
            sample_val_max (int): Maximum for range of values to randomly generate (Default: 10000)
            
        Reutrns:
            dataframe with two columns ['uid', 'value']
        '''
        
        rs_min, rs_max = rs_size
        uids, vals = self._GenSampleDataSet(sample_size=randint(rs_min, rs_max), 
                sample_val_min=sample_val_min, sample_val_max=sample_val_max)
        df = pd.DataFrame({
                'uid':uids,
                'value':vals
                })
        

        self.data = df
    
    ''' Generate Sample Data '''
    def _GenSampleDataSet(self, sample_size:int=None, 
            sample_val_min:int=None, sample_val_max:int=None):
        '''
        Generate a sample data set
        
        Arguments:
            sample_size (int):      record quantity to return
            
            sample_val_min (int):   floor of the random values
            
            sample_val_max (int):   ceiling of te random values
    
        
        Returns:
            uids (list):    list of unique identifiers
            
            values (list):  list of the random values
            
        '''
        if sample_size is None:
            sample_size = self.sample_size
        if sample_val_max is None:
            sample_val_max = self.sample_val_max
        if sample_val_min is None:
            sample_val_min = self.sample_val_min

        
        uid_len = self._DetermineUID_Length(sample_size)
        uids = []
        values = []
        for i in range(sample_size):
            key = self._GetUIDkey(uid_len, uids)
            uids.append(key)
            values.append(randint(sample_val_min, sample_val_max))
        return uids, values
    
    def _DetermineUID_Length(self, data_len:int):
        '''
        Calculate length of key needed to support the dataset without risk of 
        duplication or running out of combinations while keeping it as small
        as possible.
        
        Arguments:
            data_len (int): Size of the dataset.
        
        Returns:
            int
        
        '''
        
        l = data_len
        n = []
        for i in range(1,6):
            if l < len(string.ascii_letters+string.digits) ** i:
                n.append(i)
        return min(n)+1
    
    
    def _GenUIDkey(self, length):
        '''
        Generate a random UID Key.
        
        Arguments:
            length (int): Size of key to return.
            
        Returns:
            string
        '''
        uid = ''.join(random.choice(string.ascii_letters+string.digits) for _ in range(length))
        return uid
    
    def _GetUIDkey(self, length, vals):
        '''
            Get a UID Key
            
            Arguments:
                length (int): Size of key.
                
                vals (list): List of current UIDs to prevent duplication.
            
            Returns:
                string
        '''
        uid = self._GenUIDkey(length)
        while uid in vals:
            uid = self._GenUIDkey(length)
        return uid