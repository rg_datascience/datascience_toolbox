import xlsxwriter
import pandas as pd
from titlecase import titlecase


class settings:
    wks_meta = {'title': '',
                'author': '',
                'subject': '',
                'manager': '',
                'company': '',
                'category': '',
                'keywords': '',
                'comments': '',
                'status': ''}


class Writer:

    def __init__(self, output_file):
        self.name_output_file = output_file
        self.writer = pd.ExcelWriter(output_file, engine='xlsxwriter')
        self.workbook = self.writer.book
        self.workbook.set_properties(settings.wks_meta)
        self.meta = settings.wks_meta

    def __rename_col_string(self, name):
        return titlecase(name.replace('_', ' '))

    def __rename_columns(self, df=None, lst=None):
        if df is not None:
            cols = df.columns.values
        elif lst is not None:
            cols = lst

        cols = [self.__rename_col_string(c) for c in cols]

        if df is not None:
            df.columns = cols
        elif lst is not None:
            return cols

    def __get_col_widths(self, dataframe):
        # First we find the maximum length of the index column
        idx_max = max([len(str(s)) for s in dataframe.index.values] + [len(str(dataframe.index.name))])
        # Then, we concatenate this to the max of the lengths of column name and its values for each column, left to right
        return [idx_max] + [max([len(str(s)) for s in dataframe[col].values] + [len(col)]) for col in dataframe.columns]


    def __fix_column_widths(self, df, worksheet, min_width:int = 0):
        for i, width in enumerate(self.__get_col_widths(df)):
            if width < min_width:
                width = min_width
            worksheet.set_column( i -1, i- 1, width)

    def __get_excel_range(self, cx: int, cy: int, rx: int = 1, ry: int = 1, col_only=False):
        _cx = xlsxwriter.utility.xl_col_to_name(cx)
        _cy = xlsxwriter.utility.xl_col_to_name(cy)

        if col_only == True:
            _range = _cx + ':' + _cy
        else:
            _x = _cx + str(rx)
            _y = _cy + str(ry)

            if _x == _y:
                _range = _x
            else:
                _range = _x + ':' + _y

        return _range

    def write_basic_table(self, df, sheet_name, index:bool = False, 
                        min_col_w: int = 0, cleanup_col_names: bool = True):
        if cleanup_col_names:
            self.__rename_columns(df)
        df.to_excel(self.writer, sheet_name=sheet_name, index=index)
        self.__fix_column_widths(df, self.writer.sheets[sheet_name], min_col_w)

    def write_formatted_table(self, df, sheet_name, cols_fmt_number: list = [],
                              col_defs: list = [], header_comments: list = [],
                              start_row: int = 2, auto_width: bool=False):
        writer = self.writer
        workbook = self.workbook

        self.__rename_columns(df)
        cols_fmt_number = self.__rename_columns(lst=cols_fmt_number)

        col_defs_names = self.__rename_columns(lst=[x[0] for x in col_defs])
        col_defs_widths = [x[1] for x in col_defs]
        col_defs_hide = [x[2].get('hide') if len(x) > 2 else None for x in col_defs]
        col_widths = dict(zip(col_defs_names, col_defs_widths))
        cols_hidden = dict(zip(col_defs_names, col_defs_hide))

        col_header_comments = self.__rename_columns(lst=[x[0] for x in header_comments])
        header_comments_ = dict(zip(col_header_comments, [x[1] for x in header_comments]))

        df.to_excel(writer, sheet_name=sheet_name, index=False, startrow=start_row)

        number_format = workbook.add_format()
        number_format.set_num_format('#,##0;[Red]-#,##0;#,##0')
        currency_format = workbook.add_format()
        currency_format.set_num_format('$#,##0;$[Red](#,##0);$#,##0')
        number_format_2dec = workbook.add_format()
        number_format_2dec.set_num_format('#0.00;[Red]-#0.00;#0.00')

        number_format_3dec = workbook.add_format()
        number_format_3dec.set_num_format('#0.000;[Red]-#0.000;#0.000')

        number_format_invRed = workbook.add_format()
        number_format_invRed.set_num_format('[Red]#,##0;-#,##0;#,##0')

        number_format_2dec_invRed = workbook.add_format()
        number_format_2dec_invRed.set_num_format('[Red]#0.00;-#0.00;#0.00')

        number_format_3dec_invRed = workbook.add_format()
        number_format_3dec_invRed.set_num_format('[Red]#0.000;-#0.000;#0.000')

        pct_1dec_format = workbook.add_format()
        pct_1dec_format.set_num_format('0.0%;[Red]-0.0%;0.0%')

        number_format_pct1dec_bad = workbook.add_format({'bg_color': '#FFC7CE',
                                                      'font_color': '#9C0006',
                                                      'num_format': '0.0%'})

        number_format_pct1dec_good = workbook.add_format({'bg_color': '#C6EFCE',
                                                       'font_color': '#006100',
                                                       'num_format': '0.0%'})


        format_dict = {'csint': number_format,
                       '2dec': number_format_2dec,
                       '3dec': number_format_3dec,
                       'currency': currency_format,
                       'pct1dec': pct_1dec_format,
                       'csint_invRed': number_format_invRed,
                       '2dec_invRed': number_format_2dec_invRed,
                       '3dec_invRed': number_format_3dec_invRed,
                       'pct1dec_good': number_format_pct1dec_good,
                       'pct1dec_bad': number_format_pct1dec_bad
                       }

        header_format = workbook.add_format({
            'bold': True,
            'text_wrap': True,
            'valign': 'top',
            'align': 'center',
            'fg_color': '#CCCCCC',
            'border': 1,
            'locked': True})

        worksheet = writer.sheets[sheet_name]

        max_row = len(df) + 1 + start_row
        max_col = len(df.columns.values)

        # Add Autofilter
        range_all = self.__get_excel_range(0, max_col - 1, 1 + start_row, max_row)
        worksheet.autofilter(range_all)

        # Freeze Panes
        worksheet.freeze_panes(1 + start_row, 0)

        # Header formatting
        for col in list(df.columns.values):
            col_num = df.columns.get_loc(col)
            header_rng = self.__get_excel_range(col_num, col_num,
                                                1 + start_row, 1 + start_row)
            worksheet.write(header_rng, col, header_format)

        # New Flexible Column Formatting
        for col in col_defs:
            try:
                bool_hide = col[2].get('hide')

                # Basic Format Define
                fmt_key = col[2].get('format')
                if fmt_key is not None:
                    fmt = format_dict[fmt_key]
                else:
                    fmt = None
                col_num = df.columns.get_loc(self.__rename_col_string(col[0]))
                temp_rng = self.__get_excel_range(col_num, col_num, 2 + start_row, max_row)

                # Function application
                try:
                    if col[2].get('function') is None:
                        pass

                    elif col[2].get('function') == 'subtotal':
                        # Subtotal Column
                        rng = self.__get_excel_range(col_num, col_num,
                                                     start_row, start_row)
                        subttl_rng = self.__get_excel_range(col_num, col_num,
                                                            start_row + 2,
                                                            max_row)
                        worksheet.write_formula(rng, '=SUBTOTAL(9,{})'.format(subttl_rng),
                                                fmt)

                    elif col[2].get('function') == 'agg_rate':
                        # custom formula for column overall
                        rng = self.__get_excel_range(col_num, col_num,
                                                     start_row, start_row)
                        numer = col[2]['numerator']
                        denom = col[2]['denominator']

                        if 'multiplier' in col[2]:
                            mult = col[2]['multiplier']
                            formula_ = '=({}/{})*{mult}'.format('{}', '{}', mult=mult)
                        else:
                            formula_ = '={}/{}'

                        col_numer = df.columns.get_loc(self.__rename_col_string(numer))
                        col_denom = df.columns.get_loc(self.__rename_col_string(denom))

                        rng_numer = self.__get_excel_range(col_numer, col_numer,
                                                           start_row, start_row)

                        rng_denom = self.__get_excel_range(col_denom, col_denom,
                                                           start_row, start_row)

                        worksheet.write_formula(rng, formula_.format(rng_numer, rng_denom), fmt)

                    # Conditional Format Application
                    cond_fmts = col[2].get('conditional')
                    if cond_fmts is not None:
                        for cf in cond_fmts:
                            _fmt_key = cf.get('format')
                            _fmt = format_dict[_fmt_key]
                            cf['format'] = _fmt
                            worksheet.conditional_format(temp_rng, cf)

                    # Hide Columns when specified
                    if bool_hide:
                        worksheet.set_column(temp_rng, None, fmt, {'hidden': True})
                    else:
                        worksheet.set_column(temp_rng, width=5, cell_format=fmt)

                except IndexError:
                    pass

            except IndexError:
                pass

        # Column Widths
        if auto_width:
            self.__fix_column_widths(df, self.writer.sheets[sheet_name])
        else:
            for col in list(df.columns.values):
                col_num = df.columns.get_loc(col)
                width_ = col_widths[col]
                hidden_ = cols_hidden[col]
                temp_rng = self.__get_excel_range(col_num, col_num, col_only=True)
                if hidden_:
                    pass
                else:
                    worksheet.set_column(temp_rng, width=width_)


        # Header Comments
        worksheet.set_comments_author(self.meta['author'])
        comment_options = {'author': self.meta['author'],
                           'visible': False}

        for k, v in header_comments_.items():
            col_num = df.columns.get_loc(k)
            header_rng = self.__get_excel_range(col_num, col_num,
                                                1 + start_row, 1 + start_row)
            worksheet.write_comment(header_rng, v, comment_options)


    def save_workbook(self):
        self.writer.save()