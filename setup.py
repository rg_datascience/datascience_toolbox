from setuptools import setup, find_packages
setup(
      name='datascience_toolbox',
      version='0.0.41a3',
      author='Reece Althoff',
      author_email='reecealthoff86@gmail.com',
      packages=find_packages(),
      url='https://gitlab.com/rg_datascience/datascience_toolbox',
      license='LICENSE.txt',
      description='Functions to assist with Data Science and Feature Engineering',
      install_requires=[
            'pandas >= 0.25.3',
            'tqdm >= 4.15.0',
            'numpy >= 1.17.3',
            'scikit-learn >= 0.22.1',
            #'fuzzywuzzy',
            #'nltk',
            #'freetds',
            'xlsxwriter',
            'titlecase',
            'holidays >= 0.10.1'
      ]  
)
