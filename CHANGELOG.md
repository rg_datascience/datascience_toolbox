# Changelog
## [0.0.35] - 2023-02-15
### Added
Added a parameter to the *geo_funcs.haversine* that adds the option to multiply the
result by the sqrt of 2 which should give a good appx of travel distance.

## [0.0.34] - 2022-07-11
### Added
*geo_funcs.gen_point_from_bearing_distance* will return a geocode based on an 
origin point, distance, and direction. Useful for generating points, for example, 
a mile apart between 2 points on a map (future feature where you can enter the 
origin and destination and get the array of points in between).


## [0.0.32] - 2021-12-13
### Added
*geo_funcs.validate_geocode* similar to `validate_us_yx` but with an additional 
parameter for scope that includes:
- 'US_wTerrirtories' is all of US and US territories
- 'US48' is the contiguous US
- 'CA' is Canada
- 'MX' is Mexico
  
## [0.0.31] - 2021-08-17
### Added
- *pd_funcs.generate_bin_labels* to help with creating bin labels for use with pd.Cut()

## [0.0.30] - 2020-04-16
### Changed
- *pd_funcs.fix_cols* now has an option to convert all to uppercase. 

## [0.0.29] - 2020-04-09
### Added
- *date_funcs.AdjacentHoliday* class to check if date is adjacent to a holiday.
Init my be slow (less than a second) but will be much faster when used on large 
datasets as it creates a cache table instead of doing the complex look up for
each date.

## [0.0.28] - 2020-04-01
### Added
- *pd_funcs.encode_cyclical_col* function added to encode cyclical data.

## [0.0.27] - 2020-03-24
### Changed
- modified *printr.printr* to have different character for enclosure.

## [0.0.26] - 2020-03-20
### Added
- *geo_funcs.convert_dms_dd* to convert PC Miler geocode output to decimal degrees.

## [0.0.25] - 2020-03-09
### Added
- Added *date_funcs.get_weekof_day* which is a modification on *get_monday_of_week*
to add flexibility on day of week to return.
### Changed
- Deprecated *date_funcs.get_monday_of_week*. This should be removed in a later 
version.

## [0.0.24] - 2020-03-03
### Changed
- Tweaked xlwriter.Writer.write_basic_table to be more flexible.

## [0.0.23] - 2020-02-20
### Added
- Added *get_canadian_provinces* function to geo_funcs

## [0.0.22] - 2020-02-18
### Changed
- Bugfix in bin_packing.PreGrouper

## [0.0.21] - 2020-02-10
### Changed
- setup.py to use setup tools instead of distutils

## [0.0.20] - 2020-02-10
### Added
- Optimization module with variations of the Bin Packing algorithm.

## [0.0.19] - 2020-02-10
### Added
- Geo and miscellaneous function modules.

## [0.0.18] - 2020-02-10
### Added
- Text functions module (text_funcs.py) starting with the multiple_replace function.
