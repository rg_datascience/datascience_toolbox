# Data Science & Feature Engineering Toolbox
Various feature engineering functions.
- ### Date Functions (date_funcs)
    - Various date related functions for feature engineering.
    - #### Methods
        - **get_monday_of_week**
            - Get the monday date for a given week. Useful for flaging weeks in terms of **Week-of** *TODO: make flexible to get any day of a given week.*
        - **next_weekday**
            - Get the date of the next defined weekday. I.e. "Get the next date that is a Friday from X date."
- ### Pandas Functions (pd_funcs)
    - Various functions to help in dataframe clean up.
    - #### Methods
        - **fix_cols**
            - Renames columns to remove non-alpha numerics and spaces.
        - **fix_date_cols**
            - When columns have data stored as a string and a name containing '(date)' *can use other regex* -- column will be renamed and type converted to dates.
- ### xlwriter
    - Rough extension of the xlsxwriter package that attempts to streamline some processes. Needs work.
- ### Printr
    - Basically a fancy line printer that has a timer built in.
- ### Text Functions (text_funcs)
    - Various methods to assist in text handling.
- ### Geo Functions (geo_funcs)
    - Various methods related to mapping, distance, and geography.
- ### Misc Functions
    - Methods that don't fit any one particular bucket.

## Install
pip install git+https://gitlab.com/rg_datascience/datascience_toolbox.git
